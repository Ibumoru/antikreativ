public class Grocery {
    public String name;
    public Date openDate;
    public int expiryTime;
    public String id;
    public Date expiryDate;
    public String type;
    public String brand;

    public Grocery(String name, Date expiryDate, String type, String brand, int expiryTime) {

    }

    public String getName() {
        return name;
    }
    public Date getOpenDate() {
        return openDate;
    }
    public int getExpiryTime() {
        return expiryTime;
    }
    public String getId() {
        return id;
    }
    public Date getExpiryDate() {
        return expiryDate;
    }
    public String getType() {
        return type;
    }
    public String getBrand() {
        return brand;
    }

    public boolean isExipred() {
      boolean isExpired = true;
      Date d = new Date();
      if (expiryDate == d) {
        isExpired = false;
      }
      return isExpired;
  }
    public boolean isOpened() {
        boolean isOpened = true;

        return isOpened;
    }

    public String toString() {
      return "Name:\t" + name + "\n Open Date:\t" + openDate + "\n Expiry Date:\t" + expiryDate;
    }
}
